Ratings Project
==================================

The project would follow the same directory structure as thejackalofjavascript tutorials follow. So please stick to that and meanwhile if you learn socket.io then we will try to migrate the site to socket io.

Tech Stack
ionic framework, ionic-material (material design ui)
angularjs
nodejs
mongodb (mongoose for interaction)
restify (we will not be using express)
restify-session
bcrypt (for encrypting passwords)


use mongolab for hosting db and heroku for hosting server side scripts
client will be hosted on Google Play Store


Running the Application (locally)
------------------------------

for running we require 3 terminals

Terminal 1

with admin permissions start mongodb

mongod --smallfiles

Terminal 2

this is for the server so that server would serve our application with appropriate apis

node server.js

Terminal 3

This is for the client application made using ionic framework 

ionic serve
var pwdMgr = require('./managePasswords');
var validateRequest = require("../auth/validateRequest");
module.exports = function (server, db,UserDb) {
    // unique index
    /*UserDb.ensureIndex({
        email: 1
    }, {
        unique: true
    });*/

    // get all the user data
    /*server.get('/api/v1/ratingapp/data/user/:id', function (req, res, next) {
        validateRequest.validate(req, res, UserDb, function () {
            UserDb.find({
                _id: db.ObjectId(req.params.id)
            }, function (err, data) {
                res.writeHead(200, {
                    'Content-Type': 'application/json; charset=utf-8'
                });
                res.end(JSON.stringify(data));
            });
        });
        return next();
    });*/

    server.post('/api/v1/ratingapp/auth/register', function (req, res, next) {
        console.log("registering.....");
        var user = req.params;
        pwdMgr.cryptPassword(user.password, function (err, hash) {
            user.password = hash;
            console.log(hash, user.password);
            var newuser = new UserDb(req.params);
            newuser.save(
                function (err, dbUser) {
                    if (err) { // duplicate key error
                        if (err.code == 11000) /* http://www.mongodb.org/about/contributors/error-codes/*/ {
                            res.writeHead(400, {
                                'Content-Type': 'application/json; charset=utf-8'
                            });
                            res.end(JSON.stringify({
                                error: err,
                                message: "A user with this email already exists"
                            }));
                        }
                    } else {
                        res.writeHead(200, {
                            'Content-Type': 'application/json; charset=utf-8'
                        });
                        // this is to make password blank so that when read on client side
                        // the password can not be obtained by any means to anyone
                        // otherwise even encrypting password we  are giving it to someone 
                        // to access
                        dbUser.password = "";
                        console.log(dbUser);
                        res.end(JSON.stringify(dbUser));
                    }
                });
        });
        return next();
    });

    server.post('/api/v1/ratingapp/auth/login', function (req, res, next) {
        var user = req.params;
        if (user.email.trim().length == 0 || user.password.trim().length == 0) {
            res.writeHead(403, {
                'Content-Type': 'application/json; charset=utf-8'
            });
            res.end(JSON.stringify({
                error: "Invalid Credentials"
            }));
        }
        UserDb.findOne({
            email: req.params.email
        }, function (err, dbUser) {
            //console.log(dbUser);

            pwdMgr.comparePassword(user.password, dbUser.password, function (err, isPasswordMatch) {

                if (isPasswordMatch) {
                    res.writeHead(200, {
                        'Content-Type': 'application/json; charset=utf-8'
                    });
                    // remove password hash before sending to the client
                    dbUser.password = "";
                    res.end(JSON.stringify(dbUser));
                } else {
                    res.writeHead(403, {
                        'Content-Type': 'application/json; charset=utf-8'
                    });
                    res.end(JSON.stringify({
                        error: "Invalid User"
                    }));
                }

            });
        });
        return next();
    });

    // get one user profile and all of that user's data
    server.get('/api/v1/ratingapp/data/user/:userid', function (req, res, next) {
        console.log("Now Here");
        validateRequest.validate(req, res, UserDB, function () {
            UserDB.findOne({
                _id: req.params.userid
            }, function (err, data) {
                //console.log(err);
                res.writeHead(200, {
                    'Content-Type': 'application/json; charset=utf-8'
                });
                res.end(JSON.stringify(data));
            });
        });
        return next();
    });

};
module.exports = function (server, db,UserDB,RevProDB, RevDB) {
    var validateRequest = require("../auth/validateRequest");

    // get all review profiles
    server.get("/api/v1/ratingapp/data/allrevprofiles", function (req, res, next) {
        console.log("IN AGAIN ");
        validateRequest.validate(req, res, UserDB, function () {
            RevProDB.find(function (err, list) {
                res.writeHead(200, {
                    'Content-Type': 'application/json; charset=utf-8'
                });
                res.end(JSON.stringify(list));
            });
        });
        return next();
    });

    // get one review profile
    server.get('/api/v1/ratingapp/data/review/:revid', function (req, res, next) {
        console.log("Now Here");
        validateRequest.validate(req, res, UserDB, function () {
            RevProDB.findOne({
                _id: req.params.revid
            }, function (err, data) {
                //console.log(err);
                res.writeHead(200, {
                    'Content-Type': 'application/json; charset=utf-8'
                });
                res.end(JSON.stringify(data));
            });
        });
        return next();
    });

    // add a review profile
    server.post('/api/v1/ratingapp/data/add/reviewprof', function (req, res, next) {
        console.log("T 3");
        validateRequest.validate(req, res, UserDB, function () {
            var item = req.params;
            var newrev = new RevProDB(req.params);

            newrev.save(
                function (err, data) {
                    res.writeHead(200, {
                        'Content-Type': 'application/json; charset=utf-8'
                    });
                    res.end(JSON.stringify(data));
                });
        });
        return next();
    });


    // add user reviews to the review profiles
    server.post('/api/v1/ratingapp/data/add/reviewprof/userrev', function (req, res, next) {
        console.log("T 3");
        validateRequest.validate(req, res, UserDB, function () {
            var item = req.params;
            var newuserrev = new RevDB(req.params);

            newuserrev.save(
                function (err, data) {
                    res.writeHead(200, {
                        'Content-Type': 'application/json; charset=utf-8'
                    });
                    res.end(JSON.stringify(data));
                });
        });
        return next();
    });

    // get all user reviews of a particular review profile
    server.get("/api/v1/ratingapp/data/alluserreviews/:revid", function (req, res, next) {
        console.log("IN AGAIN ", req.params.revid);
        validateRequest.validate(req, res, UserDB, function () {
            RevDB.find({
                reviewprofileid: req.params.revid
            },function (err, list) {
                console.log("Yeah", list, err);
                res.writeHead(200, {
                    'Content-Type': 'application/json; charset=utf-8'
                });
                res.end(JSON.stringify(list));
            });
        });
        return next();
    });

    /*server.put('/api/v1/ratingapp/data/review/:id', function (req, res, next) {
        validateRequest.validate(req, res, db, function () {
            db.bucketLists.findOne({
                _id: db.ObjectId(req.params.id)
            }, function (err, data) {
                // merge req.params/product with the server/product

                var updProd = {}; // updated products 
                // logic similar to jQuery.extend(); to merge 2 objects.
                for (var n in data) {
                    updProd[n] = data[n];
                }
                for (var n in req.params) {
                    if (n != "id")
                        updProd[n] = req.params[n];
                }
                db.bucketLists.update({
                    _id: db.ObjectId(req.params.id)
                }, updProd, {
                    multi: false
                }, function (err, data) {
                    res.writeHead(200, {
                        'Content-Type': 'application/json; charset=utf-8'
                    });
                    res.end(JSON.stringify(data));
                });
            });
        });
        return next();
    });

    server.del('/api/v1/ratingapp/data/review/:id', function (req, res, next) {
        validateRequest.validate(req, res, db, function () {
            db.bucketLists.remove({
                _id: db.ObjectId(req.params.id)
            }, function (err, data) {
                res.writeHead(200, {
                    'Content-Type': 'application/json; charset=utf-8'
                });
                res.end(JSON.stringify(data));
            });
            return next();
        });
    });
*/
}
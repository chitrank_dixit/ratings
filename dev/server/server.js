// This is server of our hybrid app
var restify     =   require('restify');
var mongoose    =   require('mongoose');
var morgan      =   require('morgan');
var server      =   restify.createServer();

// mongolab link mongodb://chitrank:liomessi@ds047612.mongolab.com:47612/findnoble
mongoose.connect('mongodb://localhost/test'); // while using locally set a local mongo db link
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (callback) {
  console.log("working fine");
});

// see this link for more better database design

// +5 karma on each rating , +15 karma on each profile add

// database schemas

// ObjectId type that can be used 
var ObjectId = mongoose.Schema.Types.ObjectId;

// Database of all the users of the app
var userdb = mongoose.Schema({
    // _id is auto generated so no need for userid
    timestamp: {
        type: String,
        required: true,
        trim: true
    },
    name: {
        type:String,
        required: false,
        trim: true
    },
    password: {
        type: String,
        required: true,
        trim: true,
    },
    firstName: {
        type: String,
        required: false,
        trim: true
    },
    lastName: {
        type: String,
        required: false,
        trim: true
    },
    email:  {
        type: String,
        required: true,
        index: true,
        trim: true
    },
    level: {
        type: String,
        required: true,
        trim: true
    },
    phone: {
        type: Number,
        required: false,
        trim: true
    },
    location:{
        type: String,
        required: false,
        trim: true
    }, 
    karma: {
        // adding game theory by karma points that would encourage people to reivew and add more profiles
        type: Number,
        required: true,
        trim: true
    },
    roles: [String]
});

userdb.index({ email: 1 , unique: true } );
/*userdb.ensureIndex({
        email: 1
    }, {
        unique: true
});*/
// model of the above database collection structure userdb

var UserDB = mongoose.model('userdb',userdb);


// profile of all the shops where people would be able to review
var revprofiledb = mongoose.Schema({
    // _id is auto generated so no need for userid
    timestamp :{
        type: String,
        required: true,
        trim: true
    }, 
    name:{
        type: String,
        required: true,
        trim: true
    }, 
    email:{
        type: String,
        required: false,
        trim: true
    }, 
    description: {
        type: String,
        required: true,
        trim: true
    },
    phone:{
        type: Number,
        required: false,
        trim: true
    }, 
    category:{
        type: String,
        required: true,
        trim: true
    }, 
    imgurl:{
        type: String,
        required: false,
        trim: true
    }, 
    address:{
        type: String,
        required: false,
        trim: true
    }, 
    location:{
        type: String,
        required: false,
        trim: true
    }, 
    likes:{
        type: Number,
        required: true,
        trim: true
    }, 
    avgrating:{
        type: Number,
        required: false,
        trim: true
    }, 
    author:{
        type: String,
        required: true,
        trim: true
    }, 
    authorid:{
        type: ObjectId, //Schema.Type.ObjectId,
        required: true,
        trim: true
    }
    
});

// model of the above database collection structure revprofiledb
var RevProDB = mongoose.model('revprofiledb',revprofiledb);

// database of all the reviews done by the user at the shops profiles
var revdb = mongoose.Schema({
    timestamp:{
        type: String,
        required: true,
        trim: true
    }, 
    title:{
        type: String,
        required: true,
        trim: true
    }, 
    description:{
        type: String,
        required: true,
        trim: true
    },  // review description
    rating:{
        type: Number,
        required: true,
        trim: true
    }, 
    reviewprofileid :{
        type: ObjectId, // just to pass Schema.Type.ObjectId,
        required: true,
        trim: true
    },
    reviewer :{
        type: ObjectId, // just to pass Schema.Type.ObjectId,
        required: true,
        trim: true
    } 

});

// model of the above database collection structure revdb
var RevDB = mongoose.model('revdb',revdb);

// setting for restify library
server.use(restify.acceptParser(server.acceptable));
server.use(restify.queryParser());
server.use(restify.bodyParser());
server.use(morgan('dev')); // LOGGER

// CORS
server.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

// server would listen to port # 9804
server.listen(process.env.PORT || 9804, function () {
    console.log("Server started @ ", process.env.PORT || 9804);
});

// these are global variables these would be used to perform 
// our various db transaction and user validations
var manageUsers =   require('./auth/manageUser')(server, db, UserDB);
var manageReviewProfile =   require('./review_profile/manageReviewProfile')(server, db,UserDB,RevProDB, RevDB);